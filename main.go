package main

import (
	"encoding/csv"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/romanyx/remember/adapter/console"
	"gitlab.com/romanyx/remember/domain"
)

var (
	format   = flag.String("format", "csv", "input format")
	variants = flag.Int("variants", 4, "number of anwers")
)

func main() {
	flag.Parse()

	if len(os.Args) < 2 {
		fmt.Println("pass file path as argument")
		return
	}

	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	input, err := readInput(file, *format)
	if err != nil {
		log.Fatal(err)
	}

	t, err := domain.GenerateTest(input, *variants)
	if err != nil {
		log.Fatal(err)
	}

	console.Start(t)
}

func readInput(
	r io.Reader,
	format string,
) (map[string]string, error) {
	switch format {
	case "csv":
		return readCsv(r)
	}

	return nil, fmt.Errorf("unsupported format")
}

func readCsv(r io.Reader) (map[string]string, error) {
	c := csv.NewReader(r)

	input := make(map[string]string)
	for {
		data, err := c.Read()
		if errors.Is(err, io.EOF) {
			break
		}

		if len(data) != 2 {
			return nil, fmt.Errorf("number of elements in row should be two")
		}

		input[data[0]] = data[1]
	}

	return input, nil
}
