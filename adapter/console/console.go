package console

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/romanyx/remember/domain"
)

// Start starts test.
func Start(test domain.Test) {
	for i, question := range test {
		fmt.Println()
		fmt.Println(question.Question)
		for i, answer := range question.Variants {
			fmt.Printf("%d. %s", i+1, answer)
			if len(question.Variants) == i+1 {
				fmt.Print("\n")
				continue
			}

			fmt.Print(" | ")
		}

		scanner := bufio.NewScanner(os.Stdin)
		for {
			fmt.Println("Enter answer: ")
			scanner.Scan()
			text := scanner.Text()
			pick, err := strconv.Atoi(text)
			if err != nil {
				fmt.Println("answer should be number")
				continue
			}

			if pick > len(question.Variants) {
				fmt.Println("answer is out of number range")
				continue
			}

			answer := question.Variants[pick-1]
			question.Answer = answer
			test[i] = question

			if answer != question.Correct {
				fmt.Println("Correct answer is: ", question.Correct)
			}
			break
		}
	}

	fmt.Println()

	var correct int
	for _, question := range test {
		if question.Answer != question.Correct {
			fmt.Println(question.Question, question.Correct)
			continue
		}

		correct++
	}

	fmt.Println("correct", correct, "out of", len(test))
}
