package domain

import (
	"fmt"
	"math/rand"
	"time"
)

// Question holds question data.
type Question struct {
	Question        string
	Variants        []string
	Correct, Answer string
}

// Test holds test questions.
type Test []Question

// GenerateTest generates test from input.
func GenerateTest(
	input map[string]string,
	variantsCount int,
) (Test, error) {
	if len(input) < variantsCount {
		return nil, fmt.Errorf("length of input cannot be less than variants count")
	}

	t := make(Test, len(input))

	allVariants := make([]string, len(input))
	var i int
	for _, answer := range input {
		allVariants[i] = answer
		i++
	}

	i = 0
	for question, answer := range input {
		q := Question{
			Question: question,
			Correct:  answer,
		}

		variants := make([]string, variantsCount)
		variants[0] = answer
		check := map[string]bool{
			answer: true,
		}

		s := rand.NewSource(time.Now().Unix())
		r := rand.New(s)
		for i := 1; i < variantsCount; i++ {
			for {
				pick := allVariants[r.Intn(len(allVariants))]
				if check[pick] {
					continue
				}

				check[pick] = true
				variants[i] = pick
				break
			}
		}

		shuffle(variants)

		q.Variants = variants

		t[i] = q
		i++
	}

	return t, nil
}

func shuffle(input []string) {
	perm := rand.Perm(len(input))
	for i, v := range perm {
		input[v], input[i] = input[i], input[v]
	}
}
