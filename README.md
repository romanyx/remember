remember allows to make test quiz from simple csv files and play them in console.

```sh
$ go install gitlab.com/romanyx/remember@latest
$ remember ./example.csv

水曜日 (すいようび)
1.  Wednesday | 2.  Thursday | 3.  Monday | 4.  Friday
Enter answer: 
1

木曜日 (もくようび)
1.  Monday | 2.  Thursday | 3.  Friday | 4.  Wednesday
Enter answer: 
2

金曜日 (きんようび)
1.  Friday | 2.  Thursday | 3.  Monday | 4.  Wednesday
Enter answer: 
1

土曜日 (どようび)
1.  Saturday | 2.  Thursday | 3.  Wednesday | 4.  Monday
Enter answer: 
1

日曜日 (にちようび)
1.  Sunday | 2.  Thursday | 3.  Monday | 4.  Wednesday
Enter answer: 
1

月曜日 (げつようび)
1.  Monday | 2.  Thursday | 3.  Wednesday | 4.  Friday
Enter answer: 
1

火曜日 (かようび)
1.  Tuesday | 2.  Thursday | 3.  Monday | 4.  Wednesday
Enter answer: 
3
Correct answer is:   Tuesday

火曜日 (かようび)  Tuesday
correct 6 out of 7
```

